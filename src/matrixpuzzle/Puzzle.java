package matrixpuzzle;

import java.util.ArrayList;

/**
 * Puzzle is a container for a puzzle to be solved.
 *
 * The puzzle contains various blocks of integers. The objective is to position
 * the blocks within a square matrix so that the blocks do not overlap and
 * the matrix is symmetric.
 *
 * The coordinate system used here assumes that the upper left corner of the
 * puzzle is location (0, 0), the upper right corner is (0, N-1) and the lower
 * right corner is (N-1, N-1).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Puzzle {
  private final int dimension;            // dimension of the puzzle square
  private final ArrayList<Block> blocks;  // the list of blocks in the puzzle
  private int min;                        // the smallest value in any block
  private int max;                        // the largest value in any block

  /**
   * Constructor of an empty puzzle.
   * @param dim the puzzle square dimension
   */
  public Puzzle(final int dim) {
    dimension = dim;
    blocks = new ArrayList<>();
    min = Integer.MAX_VALUE;
    max = Integer.MIN_VALUE;
  }

  /**
   * Adds a block to the puzzle.
   * @param block the block to add
   */
  public void add(final Block block) {
    blocks.add(block);
    min = Math.min(min, block.getMin());
    max = Math.max(max, block.getMax());
  }

  /**
   * Gets the dimension of the puzzle square.
   * @return the side length of the square
   */
  public int getDimension() {
    return dimension;
  }

  /**
   * Gets the number of blocks in the puzzle.
   * @return the block count
   */
  public int getnBlocks() {
    return blocks.size();
  }

  /**
   * Gets a particular block.
   * @param n the index of the block
   * @return the requested block
   */
  public Block getBlock(final int n) {
    return blocks.get(n);
  }

  /**
   * Gets the smallest value in any block.
   * @return the smallest value in the puzzle
   */
  public int getMin() {
    return min;
  }

  /**
   * Gets the largest value in any block.
   * @return the largest value in the puzzle
   */
  public int getMax() {
    return max;
  }

  /**
   * Checks whether a block will fit at a particular location.
   * @param b the index of the block
   * @param x the row index of the upper left corner of the block
   * @param y the column index of the upper left corner of the block
   * @return true if the block at that position fits within the square
   */
  public boolean fitsAt(final int b, final int x, final int y) {
    Block block = blocks.get(b);
    return (x + block.getnRows() <= dimension)
           && (y + block.getnCols() <= dimension);
  }

  /**
   * Tests whether a particular cell will be inside a block given the
   * hypothetical location of the upper left corner of the block.
   * @param b the index of the block
   * @param x the row where the upper left corner will be located
   * @param y the column where the upper left corner will be located
   * @param i the row index of the target cell
   * @param j the column index of the target cell
   * @return true if the target cell would be located inside the block
   */
  public boolean inBlock(final int b, final int x, final int y,
                         final int i, final int j) {
    Block block = blocks.get(b);
    int r = block.getnRows();
    int c = block.getnCols();
    return (i >= x) && (i < x + r) && (j >= y) && (j < y + c);
  }

  /**
   * Gets the entry in a particular cell if a particular block is covering
   * that cell.
   * @param b the block index
   * @param x the row containing the block's upper left corner
   * @param y the column containing the block's upper left corner
   * @param i the row index of the target cell
   * @param j the column index of the target cell
   * @return the conditional content of the target cell
   */
  public int entryAt(final int b, final int x, final int y,
                     final int i, final int j) {
    if (inBlock(b, x, y, i, j)) {
      Block block = blocks.get(b);
      return block.get(i - x, j - y);
    } else {
      return 0;
    }
  }
}
