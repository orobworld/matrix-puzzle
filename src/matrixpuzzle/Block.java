package matrixpuzzle;

import java.util.Arrays;

/**
 * Block is a container for one block in a puzzle.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Block {
  private final int nRows;         // row dimension
  private final int nCols;         // column dimension
  private final int[][] contents;  // contents of the block
  private final int min;           // smallest entry in the block
  private final int max;           // largest entry in the block

  /**
   * Constructor.
   * @param block an array of integers to be stored in the block
   */
  public Block(final int[][] block) {
    nRows = block.length;
    nCols = block[0].length;
    contents = new int[nRows][nCols];
    for (int r = 0; r < nRows; r++) {
      contents[r] = Arrays.copyOf(block[r], nCols);
    }
    int lo = Integer.MAX_VALUE;
    int hi = Integer.MIN_VALUE;
    for (int[] row : contents) {
      for (int x : row) {
        lo = Math.min(lo, x);
        hi = Math.max(hi, x);
      }
    }
    min = lo;
    max = hi;
  }

  /**
   * Gets the row dimension of the block.
   * @return the row dimension
   */
  public int getnRows() {
    return nRows;
  }

  /**
   * Gets the column dimension of the block.
   * @return the column dimension
   */
  public int getnCols() {
    return nCols;
  }

  /**
   * Gets one entry in the block.
   * @param r the row index of the entry
   * @param c the column index of the entry
   * @return the block entry
   */
  public int get(final int r, final int c) {
    return contents[r][c];
  }

  /**
   * Gets the smallest integer in the block.
   * @return the smallest entry
   */
  public int getMin() {
    return min;
  }

  /**
   * Gets the largest integer in the block.
   * @return the largest entry
   */
  public int getMax() {
    return max;
  }

  /**
   * Prints the block contents.
   */
  public void print() {
    MatrixPuzzle.printArray(contents);
  }
}
