package matrixpuzzle;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 * MIP implements a mixed integer programming model to solve a puzzle.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP implements AutoCloseable {
  private static final double HALF = 0.5;  // for rounding solution values
  private final Puzzle puzzle;      // the puzzle to solve
  private final IloCplex mip;       // the MIP model
  private final IloNumVar[][][] x;  // indicators for block positions
  private final IloNumVar[][] y;    // cell contents

  /**
   * Constructor.
   * @param p the puzzle to solve
   * @throws IloException if the MIP model cannot be constructed.
   */
  public MIP(final Puzzle p) throws IloException {
    puzzle = p;
    // Get the puzzle dimensions.
    int dim = puzzle.getDimension();
    int nBlocks = puzzle.getnBlocks();
    int min = puzzle.getMin();
    int max = puzzle.getMax();
    // Initialize the MIP model.
    mip = new IloCplex();
    // Define the variables.
    x = new IloNumVar[nBlocks][dim][dim];
    for (int r = 0; r < dim; r++) {
      for (int c = 0; c < dim; c++) {
        for (int b = 0; b < nBlocks; b++) {
          // x[b][r][c] = 1 iff block b has upper left corner at (r, c)
          x[b][r][c] = mip.boolVar("x_" + r + "_" + c + "_" + b);
          // If the block does not fit there, force the variabel to be zero
          // and let the presolver remove it.
          if (!puzzle.fitsAt(b, r, c)) {
            x[b][r][c].setUB(0);
          }
        }
      }
    }
    y = new IloNumVar[dim][dim];
    for (int r = 0; r < dim; r++) {
      for (int c = 0; c < dim; c++) {
        // y[r][c] is the content of cell (r, c)
        y[r][c] = mip.numVar(min, max, "y_" + r + "_" + c);
      }
    }
    // Constraint: every block is placed exactly once.
    for (int b = 0; b < nBlocks; b++) {
      IloNumExpr expr = mip.numExpr();
      for (int r = 0; r < dim; r++) {
        expr = mip.sum(expr, mip.sum(x[b][r]));
      }
      mip.addEq(expr, 1.0, "Place_block_" + b);
    }
    // For each cell (r, c), find the conditional value of that cell and
    // the list of block locations that might cover it.
    for (int r = 0; r < dim; r++) {
      for (int c = 0; c < dim; c++) {
        IloLinearNumExpr expr1 = mip.linearNumExpr();
        IloLinearNumExpr expr2 = mip.linearNumExpr();
        for (int b = 0; b < nBlocks; b++) {
          for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
              expr1.addTerm(puzzle.entryAt(b, i, j, r, c), x[b][i][j]);
              if (puzzle.inBlock(b, i, j, r, c)) {
                expr2.addTerm(1.0, x[b][i][j]);
              }
            }
          }
        }
        // Constraint: define y[r][c].
        mip.addEq(y[r][c], expr1, "define_y_" + r + "_" + c);
        // Constraint: cell (r, c) must be covered exactly once.
        mip.addEq(expr2, 1.0, "cover_" + r + "_" + c + "_once");
      }
    }
    // Constraint: the final matrix must be symmetric.
    for (int r = 0; r < dim; r++) {
      for (int c = r + 1; c < dim; c++) {
        mip.addEq(y[r][c], y[c][r], "symmetry_" + r + "_" + c);
      }
    }
    // Since this is a feasibility problem, we use the default objective
    // (minimize 0).
  }

  /**
   * Solves the model.
   * @param timeLimit the run time limit in seconds
   * @return true if the model is solved
   * @throws IloException if CPLEX blows up
   */
  public boolean solve(final double timeLimit) throws IloException {
    // Set the time limit.
    mip.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    // Try to solve the model.
    return mip.solve();
  }

  /**
   * Gets the positions of the blocks in the final solution.
   * @return a matrix giving the row and column indices of the upper left
   * corner of each block
   * @throws IloException if the solution cannot be recovered
   */
  public int[][] getSolution() throws IloException {
    int nBlocks = puzzle.getnBlocks();
    int dim = puzzle.getDimension();
    int[][] where = new int[nBlocks][2];
    for (int b = 0; b < nBlocks; b++) {
      rloop:
      for (int r = 0; r < dim; r++) {
        double[] v = mip.getValues(x[b][r]);
        for (int c = 0; c < dim; c++) {
          if (v[c] > HALF) {
            // Block b is anchored at (r, c).
            where[b][0] = r;
            where[b][1] = c;
            break rloop;
          }
        }
      }
    }
    return where;
  }

  /**
   * Gets the final matrix.
   * @return the assembled matrix
   * @throws IloException if the solution cannot be recovered
   */
  public int[][] getMatrix() throws IloException {
    int dim = puzzle.getDimension();
    int[][] matrix = new int[dim][dim];
    for (int r = 0; r < dim; r++) {
      for (int c = 0; c < dim; c++) {
        matrix[r][c] = (int) Math.round(mip.getValue(y[r][c]));
      }
    }
    return matrix;
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    mip.close();
  }
}
