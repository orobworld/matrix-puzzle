package matrixpuzzle;

import ilog.concert.IloException;
import java.util.Arrays;

/**
 * MatrixPuzzle uses a MIP model and a CP model to solve a matrix block puzzle
 * posted on Mathematics Stack Exchange.
 *
 * Original post:
 * https://math.stackexchange.com/questions/4686170/placing-number-blocks
 * -so-that-the-resulting-matrix-is-symmetric
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MatrixPuzzle {

  /** Dummy constructor. */
  private MatrixPuzzle() { }

  /**
   * Builds and solves a puzzle.
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Create the first puzzle from the M.SE post.
    Puzzle puzzle = example1();
    // Set a time limit in seconds.
    double timeLimit = 60;
    // Try to solve it via MIP.
    System.out.println("\nSolving puzzle 1 via MIP.");
    runMIP(puzzle, timeLimit);
    // Try to solve it via CP.
    System.out.println("\nSolving puzzle 1 via CP.");
    runCP(puzzle, timeLimit);
    // Repeat with the second puzzle in the post.
    puzzle = example2();
    System.out.println("\nSolving puzzle 2 via MIP.");
    runMIP(puzzle, timeLimit);
    System.out.println("\nSolving puzzle 2 via CP.");
    runCP(puzzle, timeLimit);
  }

  /**
   * Prints an array.
   * @param array the array to print
   */
  public static void printArray(final int[][] array) {
    for (int[] row : array) {
      System.out.println(Arrays.toString(row));
    }
  }

  /**
   * Creates the first puzzle from the Math SE post.
   * @return the first puzzle
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  private static Puzzle example1() {
    Puzzle puzzle = new Puzzle(4);
    puzzle.add(new Block(new int[][] {{7}, {4}, {0}}));
    puzzle.add(new Block(new int[][] {{7, 4}}));
    puzzle.add(new Block(new int[][] {{5, 1}}));
    puzzle.add(new Block(new int[][] {{1, 8}}));
    puzzle.add(new Block(new int[][] {{3}, {9}, {2}}));
    puzzle.add(new Block(new int[][] {{6, 3, 9, 2}}));
    return puzzle;
  }

  /**
   * Creates the second puzzle from the Math SE post.
   * @return the second puzzle
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  private static Puzzle example2() {
    Puzzle puzzle = new Puzzle(5);
    puzzle.add(new Block(new int[][] {{8, 3, 4}}));
    puzzle.add(new Block(new int[][] {{4, 7}}));
    puzzle.add(new Block(new int[][] {{3, 9}}));
    puzzle.add(new Block(new int[][] {{3, 1}}));
    puzzle.add(new Block(new int[][] {{9}, {6}}));
    puzzle.add(new Block(new int[][] {{8}, {3}}));
    puzzle.add(new Block(new int[][] {{6}, {7}}));
    puzzle.add(new Block(new int[][] {{3}, {9}, {8}}));
    puzzle.add(new Block(new int[][] {{9}, {3}, {3}}));
    puzzle.add(new Block(new int[][] {{4}, {4}}));
    puzzle.add(new Block(new int[][] {{9, 3}}));
    return puzzle;
  }

  /**
   * Tries to solve a puzzle using a MIP model.
   * @param puzzle the puzzle to solve
   * @param timeLimit the limit on run time (in seconds)
   */
  private static void runMIP(final Puzzle puzzle, final double timeLimit) {
    try (MIP mip = new MIP(puzzle)) {
      // Try to solve the model.
      if (mip.solve(timeLimit)) {
        int[][] where = mip.getSolution();
        for (int b = 0; b < puzzle.getnBlocks(); b++) {
          System.out.println("Block at (" + where[b][0] + ", "
                             + where[b][1] + "):");
          puzzle.getBlock(b).print();
        }
        int[][] matrix = mip.getMatrix();
        System.out.println("\nFinal matrix:");
        printArray(matrix);
      }
    } catch (IloException ex) {
      System.out.println("Things went splat:\n" + ex.getMessage());
    }
  }

  /**
   * Tries to solve a puzzle using a CP model.
   * @param puzzle the puzzle to solve
   * @param timeLimit the run time limit (in seconds)
   */
  private static void runCP(final Puzzle puzzle, final double timeLimit) {
    try (CP cp = new CP(puzzle)) {
      // Try to solve the model.
      if (cp.solve(timeLimit)) {
        int[][] where = cp.getSolution();
        for (int b = 0; b < puzzle.getnBlocks(); b++) {
          System.out.println("Block at (" + where[b][0] + ", "
                             + where[b][1] + "):");
          puzzle.getBlock(b).print();
        }
        int[][] matrix = cp.getMatrix();
        System.out.println("\nFinal matrix:");
        printArray(matrix);
      }
    } catch (IloException ex) {
      System.out.println("Things went splat:\n" + ex.getMessage());
    }
  }

}
