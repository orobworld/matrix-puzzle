package matrixpuzzle;

import ilog.concert.IloException;
import ilog.concert.IloIntExpr;
import ilog.concert.IloIntVar;
import ilog.cp.IloCP;

/**
 * CP implements a constraint programming model to solve a puzzle.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class CP implements AutoCloseable {
  private final Puzzle puzzle;      // the puzzle to solve
  private final IloCP cp;           // the CP model
  private final IloIntVar[] x;      // abscissas of upper left corner of blocks
  private final IloIntVar[] y;      // ordinates of upper left corner of blocks
  // The next two vectors are actually N x N arrays (N = puzzle dimension)
  // flattened using row-first scan, in order to allow us to use the element()
  // operator.
  private final IloIntVar[] matrix;     // entries in final matrix
  private final IloIntVar[] coveredBy;  // index of block covering each entry

  /**
   * Constructor.
   * @param p the puzzle to solve
   * @throws IloException if the model cannot be constructed
   */
  public CP(final Puzzle p) throws IloException {
    puzzle = p;
    // Get the puzzle dimensions.
    int dim = puzzle.getDimension();
    int nBlocks = puzzle.getnBlocks();
    int min = puzzle.getMin();
    int max = puzzle.getMax();
    // Initialize the CP model.
    cp = new IloCP();
    // Create the variables.
    x = new IloIntVar[nBlocks];
    y = new IloIntVar[nBlocks];
    matrix = new IloIntVar[dim * dim];
    coveredBy = new IloIntVar[dim * dim];
    for (int b = 0; b < nBlocks; b++) {
      Block block = puzzle.getBlock(b);
      // x and y bounds are set so that the block fits in the square.
      x[b] = cp.intVar(0, dim - block.getnRows(), "x_" + b);
      y[b] = cp.intVar(0, dim - block.getnCols(), "y_" + b);
    }
    for (int r = 0; r < dim; r++) {
      for (int c = 0; c < dim; c++) {
        matrix[dim * r + c] = cp.intVar(min, max, "matrix_" + r + "_" + c);
        coveredBy[dim * r + c] =
          cp.intVar(0, nBlocks - 1, "coveredBy_" + r + "_" + c);
      }
    }
    // We add constraints for every combination of a block and a possible
    // location for its upper left corner.
    for (int b = 0; b < nBlocks; b++) {
      Block block = puzzle.getBlock(b);
      int bRows = block.getnRows();
      int bCols = block.getnCols();
      // Look at each cell covered by the block.
      for (int r = 0; r < bRows; r++) {
        for (int c = 0; c < bCols; c++) {
          // Flatten the cell coordinates into a 1D index.
          IloIntExpr index = cp.prod(dim, cp.sum(x[b], r));
          index = cp.sum(index, cp.sum(y[b], c));
          // Constraint: the cell with that index is covered by the block.
          cp.addEq(cp.element(coveredBy, index), b,
                   "covering_" + b + "_" + r + "_" + c);
          // Constraint: the value of that cell is determined by the block.
          cp.addEq(cp.element(matrix, index), block.get(r, c),
                   "value_" + b + "_" + r + "_" + c);
        }
      }
    }
    // Constraint: the final matrix must be symmetric.
    for (int r = 0; r < dim; r++) {
      for (int c = r + 1; c < dim; c++) {
        cp.addEq(matrix[dim * r + c], matrix[dim * c + r],
                 "symmetry_" + r + "_" + c);
      }
    }
  }

  /**
   * Solves the model.
   * @param timeLimit the run time limit in seconds
   * @return true if the model is solved
   * @throws IloException if CP Optimizer blows up
   */
  public boolean solve(final double timeLimit) throws IloException {
    cp.setParameter(IloCP.DoubleParam.TimeLimit, timeLimit);
    return cp.solve();
  }

  /**
   * Gets the positions of the blocks in the final solution.
   * @return a matrix giving the row and column indices of the upper left
   * corner of each block
   * @throws IloException if the solution cannot be recovered
   */
  public int[][] getSolution() throws IloException {
    int nBlocks = puzzle.getnBlocks();
    int[][] where = new int[nBlocks][2];
    for (int b = 0; b < nBlocks; b++) {
      where[b][0] = cp.getValue("x_" + b);
      where[b][1] = cp.getValue("y_" + b);
    }
    return where;
  }

  /**
   * Gets the final matrix.
   * @return the assembled matrix
   * @throws IloException if the solution cannot be recovered
   */
  public int[][] getMatrix() throws IloException {
    int dim = puzzle.getDimension();
    int[][] m = new int[dim][dim];
    for (int r = 0; r < dim; r++) {
      for (int c = 0; c < dim; c++) {
        m[r][c] = cp.getValue("matrix_" + r + "_" + c);
      }
    }
    return m;
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    cp.end();
  }

}
