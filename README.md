# Matrix Puzzle

This repository contains a Java program that uses CPLEX and CP Optimizer to solve a puzzle in which a given set of blocks (submatrices) need to be positioned within a matrix to make the overall matrix symmetric. See the [original question](https://math.stackexchange.com/questions/4686170/placing-number-blocks-so-that-the-resulting-matrix-is-symmetric) on Mathematics Stack Exchange for details of the problem and this [PDF file](https://rubin.msu.domains/blog/matrix-puzzle-formulations.pdf), a copy of which is included in the repository, for documentation of the two models tested. A [brief write-up](https://orinanobworld.blogspot.com/2023/04/a-matrix-puzzle.html) also appears on my blog.

The program requires two external libraries:

* CPLEX (tested with version 22.1.1); and
* CP Optimizer (also tested with version 22.1.1).


